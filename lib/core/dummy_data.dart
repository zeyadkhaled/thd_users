class DummyData {
  static List<String> nationality = const [
    'Saudi Arabia',
    'Arab Expat',
    'Non-Arab Expat',
    'GCC National'
  ];

  static List<String> smoke = const [
    'Belmont',
    'Cleopatra',
    'Merit',
    'Merit 5mg',
    'Merit 8mg',
    'Merit Filter 8mg',
    'Rothmans Switch',
    'Karelia Slims',
    'Kent Nanotek 1mg',
    'Parliament Silver Blue',
    'Rothmans King Size',
  ];

  static List<String> smokeDavidoff = const [
    'Davidoff Aniversario',
    'Davidoff Signature',
    'Davidoff Nicaragua',
    'Davidoff Millennium',
    'Davidoff Gold',
    'Davidoff Winston Churchill',
    'Davidoff Classic',
    'Davidoff Yamasa',
    'Davidoff Nicaragua Toro',
    'Davidoff Dominicana',
    'Davidoff Primeros',
  ];

  static List<String> city = [
    'Abu Dhabi',
    'Ajman',
    'Al Ain',
    'Buraidah',
    'Dammam',
    'Dubai',
    'Fujairah',
    'Hail',
    'Hassa',
    'Jeddah',
    'Jizan',
    'Jubeil',
    'Khamis',
    'Ras Al Khaimah',
    'Riyadh',
    'Sakakah',
    'Sharjah',
    'Tabuk',
    'Taif',
    'Other'
  ];

  static List<String> age = ['18-24', '25-34', '35-44', '45+'];
}
